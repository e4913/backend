﻿namespace Events.Events;

public static class EventConstants
{
    public const string DefaultSorting = "Title desc";
    
    public const int MaxTitleLength = 100;
    public const int MinTitleLength = 10;

    public const int MaxDescriptionLength = 511;
    public const int MinDescriptionLength = 100;
}