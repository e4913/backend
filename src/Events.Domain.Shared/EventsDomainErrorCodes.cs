﻿namespace Events;

public static class EventsDomainErrorCodes
{
    public const string OrganizerNameAlreadyExists = "Event:OrganizerNameAlreadyExists";
    public const string EventTitleAlreadyExists = "Event:EventTitleAlreadyExists";
    public const string NotAllowToUpdateOrganizer = "Event:NotAllowToUpdateOrganizer";
}
