﻿using Volo.Abp.Localization;

namespace Events.Localization;

[LocalizationResourceName("Events")]
public class EventsResource
{

}
