﻿namespace Events.Organizers;

public static class OrganizerConstants
{
    public const int MaxNameLength = 255;
    public const int MinNameLength = 4;

    public const int MaxDescriptionLength = 511;
    public const int MinDescriptionLength = 100;
}