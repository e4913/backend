﻿using Events.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Events.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(EventsEntityFrameworkCoreModule),
    typeof(EventsApplicationContractsModule)
    )]
public class EventsDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
