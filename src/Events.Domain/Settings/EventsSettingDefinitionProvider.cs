﻿using Volo.Abp.Settings;

namespace Events.Settings;

public class EventsSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(EventsSettings.MySetting1));
    }
}
