﻿using System;
using Volo.Abp;
using Volo.Abp.Domain.Entities.Auditing;

namespace Events.Organizers;

public class Organizer : FullAuditedAggregateRoot<Guid>
{
    public Guid UserId { get; private set; }
    public string Name { get; private set; }
    public string Description { get; private set; }
    
    protected Organizer() {}

    internal Organizer(Guid id, Guid userId, string name, string description) : base(id)
    {
        UserId = userId;
        SetName(name);
        SetDescription(description);
    }

    public void SetName(string name) => 
        Name = Check.NotNullOrWhiteSpace(name, nameof(name), OrganizerConstants.MaxNameLength, OrganizerConstants.MinNameLength);
    
    public void SetDescription(string description) =>
        Description = Check.NotNullOrWhiteSpace(description, nameof(description), OrganizerConstants.MaxDescriptionLength, OrganizerConstants.MinDescriptionLength);
}