﻿using System;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;

namespace Events.Organizers;

public class OrganizerManager : DomainService
{
    private readonly IRepository<Organizer, Guid> _organizerRepository;

    public OrganizerManager(IRepository<Organizer, Guid> organizerRepository)
    {
        _organizerRepository = organizerRepository;
    }

    public async Task<Organizer> CreateAsync(Guid userId, string name, string description)
    {
        if (await _organizerRepository.AnyAsync(x => x.Name == name))
        {
            throw new BusinessException(EventsDomainErrorCodes.OrganizerNameAlreadyExists)
                .WithData(nameof(name), name);
        }

        return new Organizer(
            GuidGenerator.Create(),
            userId,
            name,
            description
        );
    }
}