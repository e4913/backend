﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Identity;

namespace Events.Organizers.Employers;

public class OrganizerEmployeeManager : DomainService
{
    private readonly IRepository<OrganizerEmployee, Guid> _organizerEmployeeRepository;

    public OrganizerEmployeeManager(IRepository<OrganizerEmployee, Guid> organizerEmployeeRepository)
    {
        _organizerEmployeeRepository = organizerEmployeeRepository;
    }

    public async Task JoinAsync(Organizer organizer, IdentityUser user)
    {
        if (await IsJoinedAsync(organizer, user))
        {
            return;
        }

        await _organizerEmployeeRepository.InsertAsync(
            new OrganizerEmployee(GuidGenerator.Create(), organizer.Id, user.Id));
    }

    public async Task<bool> IsJoinedAsync(Organizer organizer, IdentityUser user)
    {
        return await _organizerEmployeeRepository
            .AnyAsync(x => x.OrganizerId == organizer.Id && x.UserId == user.Id);
    }
}