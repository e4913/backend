﻿using System;
using Volo.Abp.Domain.Entities;

namespace Events.Organizers.Employers;

public class OrganizerEmployeeWithDetails
{
    public Guid Id { get; set; }
    public string OrganizerName { get; set; } = default!;
    public string EmployeeUserName { get; set; } = default!;
}