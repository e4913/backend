using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Events.Organizers.Employers;

public class OrganizerEmployee : CreationAuditedEntity<Guid>
{
    public Guid OrganizerId { get; private set; }
    public Guid UserId { get; private set; }

    protected OrganizerEmployee()
    {
    }

    internal OrganizerEmployee(Guid id, Guid organizerId, Guid userId) : base(id)
    {
        OrganizerId = organizerId;
        UserId = userId;
    }
}