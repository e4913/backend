﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;

namespace Events.Organizers.Employers;

public interface IOrganizerEmployeeRepository : IRepository<OrganizerEmployee, Guid>
{
    Task<int> GetCountAsync(
        Guid? organizerId,
        Guid? userId,
        CancellationToken cancellationToken = default);
    
    Task<List<OrganizerEmployeeWithDetails>> GetListAsync(
        Guid? organizerId,
        Guid? userId,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default);

    Task<(int, List<IdentityUser>)> GetEmployersAsync(
        Guid organizerId,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default);
}