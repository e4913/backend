﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Events.Organizers;

public interface IOrganizerRepository : IRepository<Organizer, Guid>
{
    Task<(int, List<Organizer>)> GetPagedListAsync(
        Guid? ownerId,
        string? name,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default
    );

    Task<List<Organizer>> GetOwnedOrganizersAsync(
        Guid ownerId,
        CancellationToken cancellationToken = default
    );
}