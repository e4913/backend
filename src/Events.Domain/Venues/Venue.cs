﻿using System;
using Volo.Abp;
using Volo.Abp.Domain.Entities;

namespace Events.Venues;

public class Venue : AggregateRoot<Guid>
{
    public string Name { get; private set; }
    
    protected Venue() {}

    internal Venue(Guid id, string name) : base(id)
    {
        Name = Check.NotNullOrWhiteSpace(name, nameof(name), VenueConstants.MaxNameLength);
    }
}