﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Values;

namespace Events.Events;

public class TimeOfWork : ValueObject
{
    public readonly Dictionary<DayOfWeek, TimeOfWorkDay> DayTimes = default!;

    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return DayTimes;
    }
}

public class TimeOfWorkDay : ValueObject
{
    public readonly Time StartTime;
    public readonly Time EndTime;

    public TimeOfWorkDay(Time startTime, Time endTime, Time? breakTime = null)
    {
        if (startTime >= endTime)
        {
            throw new ArgumentException("Start time can't be greater or equal end time");
        }

        if (breakTime != null)
        {
            if (startTime >= breakTime)
            {
                throw new ArgumentException("Start time can't be greater or equal end time");
            }

            if (breakTime >= endTime)
            {
                throw new ArgumentException("Break time can't be greater or equal end time");
            }
        }

        StartTime = startTime;
        EndTime = endTime;
    }

    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return StartTime;
        yield return EndTime;
    }
}

public class Time : ValueObject, IComparable<Time>
{
    public readonly byte Hour;
    public readonly byte Minutes;

    public Time(byte hour, byte minutes)
    {
        if (hour is > 23 or < 0)
        {
            throw new ArgumentException("Hour can't be greater then 23 and lower than 0");
        }

        if (minutes is > 59 or < 0)
        {
            throw new ArgumentException("Minutes can't be greater then 23 and lower than 0");
        }

        Hour = hour;
        Minutes = minutes;
    }

    public int CompareTo(Time other)
    {
        var hourComparison = Hour.CompareTo(other.Hour);
        return hourComparison != 0 ? hourComparison : Minutes.CompareTo(other.Minutes);
    }

    public static bool operator >(Time left, Time right)
    {
        return left.CompareTo(right) > 0;
    }

    public static bool operator >=(Time left, Time right)
    {
        return left.CompareTo(right) >= 0;
    }

    public static bool operator <(Time left, Time right)
    {
        return left.CompareTo(right) < 0;
    }

    public static bool operator <=(Time left, Time right)
    {
        return left.CompareTo(right) <= 0;
    }

    protected override IEnumerable<object> GetAtomicValues()
    {
        yield return Hour;
        yield return Minutes;
    }
}