using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Events.Events;

public interface IEventRepository : IRepository<Event, Guid>
{
    Task<int> GetCountAsync(
        string? title = null,
        string? organizerName = null,
        string? venueName = null,
        CancellationToken cancellationToken = default
    );

    Task<(int, List<EventWithDetails>)> GetPagedListAsync(
        string? sorting = null,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        string? title = null,
        string? organizerName = null,
        string? venueName = null,
        CancellationToken cancellationToken = default
    );
}