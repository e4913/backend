﻿using System;
using Volo.Abp.Domain.Entities;

namespace Events.Events;

public class EventCategory : Entity
{
    public Guid EventId { get; private set; }
    public Guid CategoryId { get; private set; }

    protected EventCategory()
    {
    }

    internal EventCategory(Guid eventId, Guid categoryId)
    {
        EventId = eventId;
        CategoryId = categoryId;
    }

    public override object[] GetKeys()
    {
        return new object[] { EventId, CategoryId };
    }
}