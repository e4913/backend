﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Events.Categories;
using Microsoft.VisualBasic;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace Events.Events;

public class Event : FullAuditedAggregateRoot<Guid>
{
    public string Title { get; private set; }
    public string Description { get; private set; }

    public Guid OrganizerId { get; private set; }
    public Guid VenueId { get; private set; }

    //todo image
    //todo work time
    public virtual ICollection<EventCategory> Categories { get; } = default!;

    protected Event()
    {
    }

    internal Event(Guid id, string title, string description, Guid organizerId, Guid venueId) : base(id)
    {
        SetTitle(title);
        SetDescription(description);
        OrganizerId = organizerId;
        VenueId = venueId;
        Categories = new Collection<EventCategory>();
    }

    internal void SetTitle(string title) =>
        Title = Check.NotNullOrWhiteSpace(title, nameof(title), EventConstants.MaxTitleLength,
            EventConstants.MinTitleLength);

    public void SetDescription(string description) =>
        Description = Check.NotNullOrWhiteSpace(description, nameof(description), EventConstants.MaxDescriptionLength,
            EventConstants.MinDescriptionLength);

    public void SetVenue(Guid venueId) =>
        VenueId = Check.NotNull(venueId, nameof(venueId));

    public void SetCategories(List<Guid> categoryIds)
    {
        if (categoryIds.IsNullOrEmpty())
        {
            RemoveAllCategories();
            return;
        }
        
        RemoveAllCategoriesExceptGivenIds(categoryIds);
        categoryIds.ForEach(AddCategory);
    }

    internal void AddCategory(Guid categoryId)
    {
        Check.NotNull(categoryId, nameof(categoryId));

        if (IsInCategory(categoryId))
        {
            return;
        }

        Categories.Add(new EventCategory(eventId: Id, categoryId));
    }

    internal void RemoveCategory(Guid categoryId)
    {
        Check.NotNull(categoryId, nameof(categoryId));

        if (!IsInCategory(categoryId))
        {
            return;
        }

        Categories.RemoveAll(x => x.CategoryId == categoryId);
    }

    internal void RemoveAllCategoriesExceptGivenIds(List<Guid> categoryIds)
    {
        Check.NotNullOrEmpty(categoryIds, nameof(categoryIds));

        Categories.RemoveAll(x => !categoryIds.Contains(x.CategoryId));
    }

    internal void RemoveAllCategories()
    {
        Categories.RemoveAll(x => x.EventId == Id);
    }
    
    private bool IsInCategory(Guid categoryId) =>
        Categories.Any(x => x.CategoryId == categoryId);
}