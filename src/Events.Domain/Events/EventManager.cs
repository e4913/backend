﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;

namespace Events.Events;

public class EventManager : DomainService
{
    private readonly IRepository<Event, Guid> _eventRepository;

    public EventManager(IRepository<Event, Guid> eventRepository)
    {
        _eventRepository = eventRepository;
    }

    public async Task<Event> CreateAsync(string title, string description, Guid organizationId, Guid venueId)
    {
        if (await _eventRepository.AnyAsync(x => x.Title.Equals(title) && !x.IsDeleted))
        {
            throw new BusinessException(EventsDomainErrorCodes.EventTitleAlreadyExists)
                .WithData(nameof(title), title);
        }
        
        return new Event(
            GuidGenerator.Create(),
            title,
            description,
            organizationId,
            venueId
        );
    }
}