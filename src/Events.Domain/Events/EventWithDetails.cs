﻿using System;

namespace Events.Events;

public class EventWithDetails
{
    public Guid Id { get; set; }
    public string Title { get; set; } = default!;
    public string Description { get; set; } = default!;
    public string OrganizerName { get; set; } = default!;
    public string VenueName { get; set; } = default!;
}