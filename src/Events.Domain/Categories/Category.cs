﻿using System;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace Events.Categories;

public class Category : AggregateRoot<Guid>
{
    public string Name { get; private set; }
    
    protected Category() {}

    internal Category(Guid id, string name) : base(id)
    {
        Name = Check.NotNullOrWhiteSpace(name, nameof(name), CategoryConstants.MaxNameLength);
    }
}