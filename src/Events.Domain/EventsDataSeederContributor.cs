﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Events.Categories;
using Events.Venues;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Guids;

namespace Events;

public class EventsDataSeederContributor : IDataSeedContributor, ITransientDependency
{
    private readonly IGuidGenerator _guidGenerator;
    private readonly IRepository<Category, Guid> _categoryRepository;
    private readonly IRepository<Venue, Guid> _venueRepository;

    public EventsDataSeederContributor(
        IGuidGenerator guidGenerator,
        IRepository<Category, Guid> categoryRepository,
        IRepository<Venue, Guid> venueRepository
    )
    {
        _guidGenerator = guidGenerator;
        _categoryRepository = categoryRepository;
        _venueRepository = venueRepository;
    }

    public async Task SeedAsync(DataSeedContext context)
    {
        await SeedCategoriesAsync();
        await SeedVenueAsync();
    }

    private async Task SeedCategoriesAsync()
    {
        if (await _categoryRepository.GetCountAsync() != 0)
        {
            return;
        }

        var categoryNames = new[]
        {
            "Ночные клубы",
            "Концерты",
            "Туризм",
            "Для котов",
            "Для кодеров на шарпах",
        };

        await _categoryRepository.InsertManyAsync(
            categoryNames
                .Select(x => new Category(_guidGenerator.Create(), x))
        );
    }

    private async Task SeedVenueAsync()
    {
        if (await _categoryRepository.GetCountAsync() != 0)
        {
            return;
        }

        var venueNames = new[]
        {
            "Горный Алтай",
            "Шерегеш",
            "Новокузнецк"
        };

        await _venueRepository.InsertManyAsync(
            venueNames
                .Select(x => new Venue(_guidGenerator.Create(), x))
        );
    }
}