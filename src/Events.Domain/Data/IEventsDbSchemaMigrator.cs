﻿using System.Threading.Tasks;

namespace Events.Data;

public interface IEventsDbSchemaMigrator
{
    Task MigrateAsync();
}
