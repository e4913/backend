﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Events.Data;

/* This is used if database provider does't define
 * IEventsDbSchemaMigrator implementation.
 */
public class NullEventsDbSchemaMigrator : IEventsDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
