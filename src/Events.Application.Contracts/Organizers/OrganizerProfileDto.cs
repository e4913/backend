using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers;

public class OrganizerProfileDto : EntityDto<Guid>
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string OwnerUserName { get; set; }
    public string OwnerEmail { get; set; }
}