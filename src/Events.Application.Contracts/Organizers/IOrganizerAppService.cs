using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Events.Organizers;

public interface IOrganizerAppService : IApplicationService
{
    Task<OrganizerDto> CreateAsync(OrganizerCreateDto organizerCreateDto);
    Task<PagedResultDto<OrganizerListElementDto>> GetListAsync(OrganizerFilterDto organizerFilterDto);
    Task<OrganizerProfileDto> GetProfileDtoAsync(string name);
    Task<ListResultDto<OrganizerListElementDto>> GetOrganizersByUserIdAsync(Guid userId);
    Task<bool> IsOrganizerOwnerAsync(Guid organizerId);
    Task UpdateAsync(Guid id, OrganizerUpdateDto organizerUpdateDto);
}