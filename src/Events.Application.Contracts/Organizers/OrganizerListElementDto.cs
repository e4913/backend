using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers;

public class OrganizerListElementDto : EntityDto<Guid>
{
    public string Name { get; set; }
    public string Description { get; set; }
}