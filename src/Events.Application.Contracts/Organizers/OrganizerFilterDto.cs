using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers;

public class OrganizerFilterDto : PagedResultRequestDto
{
    public Guid? OwnerId { get; set; }
    public string? Name { get; set; }
}