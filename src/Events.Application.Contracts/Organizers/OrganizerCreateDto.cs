using System.ComponentModel.DataAnnotations;

namespace Events.Organizers;

public class OrganizerCreateDto
{
    [Required]
    [StringLength(OrganizerConstants.MaxNameLength, MinimumLength = OrganizerConstants.MinNameLength)]
    public string Name { get; set; }
    
    [Required]
    [StringLength(OrganizerConstants.MaxDescriptionLength, MinimumLength = OrganizerConstants.MinDescriptionLength)]
    public string Description { get; set; }
}