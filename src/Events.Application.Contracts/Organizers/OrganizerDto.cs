using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers;

public class OrganizerDto : EntityDto<Guid>
{
    public string Name { get; set; } = default!;
}