﻿using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers.Employers;

public class OrganizerEmployeeDto : EntityDto<Guid>
{
    public string EmployeeUserName { get; set; } = default!;
}