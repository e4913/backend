﻿using System;
using Volo.Abp.Application.Dtos;

namespace Events.Organizers.Employers;

public class OrganizerEmployeeFilterDto : PagedResultRequestDto
{
    public Guid OrganizerId { get; set; }
}