﻿using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Events.Organizers.Employers;

public interface IOrganizerEmployeeAppService : IApplicationService
{
    Task JoinAsync(Guid organizerId);
    Task LeaveAsync(Guid organizerId);
    Task<bool> IsJoinedAsync(Guid organizerId);
    Task<PagedResultDto<OrganizerEmployeeDto>> GetEmployersAsync(OrganizerEmployeeFilterDto organizerFilterDto);
}