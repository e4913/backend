﻿using System;
using Volo.Abp.Application.Dtos;

namespace Events.Venues;

public class VenueLookupDto : EntityDto<Guid>
{
    public string Name { get; set; }
}