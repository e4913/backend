﻿using Events.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Events.Permissions;

public class EventsPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(EventsPermissions.GroupName);
        //Define your own permissions here. Example:
        //myGroup.AddPermission(EventsPermissions.MyPermission1, L("Permission:MyPermission1"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<EventsResource>(name);
    }
}
