﻿using Volo.Abp.Application.Dtos;

namespace Events.Categories;

public class CreateUpdateCategoryDto
{
    public string Name { get; set; }
}