using System;
using Volo.Abp.Application.Dtos;

namespace Events.Events;

public class EventDto : EntityDto<Guid>
{
    public Guid OrganizerId { get; set; }
    public Guid VenueId { get; set; }
    
    public string Title { get; set; }
    public string Description { get; set; }
}