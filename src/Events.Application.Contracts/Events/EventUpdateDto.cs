using System;

namespace Events.Events;

public class EventUpdateDto
{
    public string Title { get; set; }
    public string Description { get; set; }
    public Guid? Venue { get; set; }
}