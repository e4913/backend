using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Events.Venues;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Events.Events;

public interface IEventAppService : IApplicationService
{
    Task<EventDto> CreateAsync(EventCreateDto eventCreateDto);

    Task<PagedResultDto<EventListElementDto>> GetListAsync(EventFilterDto eventFilterDto);

    Task<List<VenueLookupDto>> GetVenuesLookupAsync();

    Task<bool> IsOwnerAsync(Guid id);

    Task UpdateAsync(Guid id, EventUpdateDto eventUpdateDto);
}