using System;
using Volo.Abp.Application.Dtos;

namespace Events.Events;

public class EventFilterDto : PagedResultRequestDto
{
    public Guid? OrganizationId { get; set; }
    public Guid? VenueId { get; set; }

    public EventFilterDto()
    {
        MaxResultCount = 11;
    }
}