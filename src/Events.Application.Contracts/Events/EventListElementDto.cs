using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;

namespace Events.Events;

public class EventListElementDto : EntityDto<Guid>, IHasCreationTime, IHasModificationTime
{
    public string OrganizerName { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Venue { get; set; }
    public DateTime CreationTime { get; set; }
    public DateTime? LastModificationTime { get; set; }
}