using System;
using System.ComponentModel.DataAnnotations;

namespace Events.Events;

public class EventCreateDto
{
    [Required]
    public Guid OrganizerId { get; set; }
    
    [Required]
    public Guid VenueId { get; set; }
    
    [Required]
    [StringLength(EventConstants.MaxTitleLength, MinimumLength = EventConstants.MinTitleLength)]
    public string Title { get; set; }
    
    [Required]
    [StringLength(EventConstants.MaxDescriptionLength, MinimumLength = EventConstants.MinDescriptionLength)]
    public string Description { get; set; }
}