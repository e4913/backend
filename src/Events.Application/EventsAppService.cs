﻿using System;
using System.Collections.Generic;
using System.Text;
using Events.Localization;
using Volo.Abp.Application.Services;

namespace Events;

/* Inherit your application services from this class.
 */
public abstract class EventsAppService : ApplicationService
{
    protected EventsAppService()
    {
        LocalizationResource = typeof(EventsResource);
    }
}
