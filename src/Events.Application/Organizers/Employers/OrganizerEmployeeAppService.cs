﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Users;

namespace Events.Organizers.Employers;

public class OrganizerEmployeeAppService : EventsAppService, IOrganizerEmployeeAppService
{
    private readonly OrganizerEmployeeManager _organizerEmployeeManager;
    private readonly IUserRepository<IdentityUser> _userRepository;
    private readonly IRepository<Organizer, Guid> _organizerRepository;
    private readonly IOrganizerEmployeeRepository _organizerEmployeeRepository;

    public OrganizerEmployeeAppService(
        OrganizerEmployeeManager organizerEmployeeManager, 
        IUserRepository<IdentityUser> userRepository, 
        IRepository<Organizer, Guid> organizerRepository, 
        IOrganizerEmployeeRepository organizerEmployeeRepository)
    {
        _organizerEmployeeManager = organizerEmployeeManager;
        _userRepository = userRepository;
        _organizerRepository = organizerRepository;
        _organizerEmployeeRepository = organizerEmployeeRepository;
    }

    public async Task JoinAsync(Guid organizerId)
    {
        await _organizerEmployeeManager.JoinAsync(
            await _organizerRepository.GetAsync(organizerId),
            await _userRepository.GetAsync(CurrentUser.GetId())
        );
    }

    public async Task LeaveAsync(Guid organizerId)
    {
        await _organizerEmployeeRepository.DeleteAsync(x =>
            x.OrganizerId == organizerId && x.UserId == CurrentUser.GetId());
    }

    public async Task<bool> IsJoinedAsync(Guid organizerId)
    {
        return await _organizerEmployeeManager.IsJoinedAsync(
            await _organizerRepository.GetAsync(organizerId),
            await _userRepository.GetAsync(CurrentUser.GetId())
        );
    }

    public async Task<PagedResultDto<OrganizerEmployeeDto>> GetEmployersAsync(OrganizerEmployeeFilterDto organizerEmployeeFilterDto)
    {
        var organizerEmployeeQueryable = await _organizerEmployeeRepository.GetQueryableAsync();
        var (totalCount, users) = await _organizerEmployeeRepository.GetEmployersAsync(
            organizerEmployeeFilterDto.OrganizerId,
            organizerEmployeeFilterDto.SkipCount,
            organizerEmployeeFilterDto.MaxResultCount
        );

        return new PagedResultDto<OrganizerEmployeeDto>(totalCount,
            ObjectMapper.Map<List<IdentityUser>, List<OrganizerEmployeeDto>>(users));
    }
}