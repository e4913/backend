﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Events.Organizers.Employers;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Authorization;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Identity;
using Volo.Abp.Users;

namespace Events.Organizers;

public class OrganizerAppService : EventsAppService, IOrganizerAppService
{
    private readonly OrganizerManager _organizerManager;
    private readonly IOrganizerRepository _organizerRepository;
    private readonly IOrganizerEmployeeRepository _organizerEmployeeRepository;
    private readonly IUserRepository<IdentityUser> _userRepository;

    public OrganizerAppService(
        OrganizerManager organizerManager, 
        IOrganizerRepository organizerRepository, 
        IOrganizerEmployeeRepository organizerEmployeeRepository, 
        IUserRepository<IdentityUser> userRepository)
    {
        _organizerManager = organizerManager;
        _organizerRepository = organizerRepository;
        _organizerEmployeeRepository = organizerEmployeeRepository;
        _userRepository = userRepository;
    }

    [Authorize]
    public async Task<OrganizerDto> CreateAsync(OrganizerCreateDto organizerCreateDto)
    {
        var organizer = await _organizerManager.CreateAsync(
            CurrentUser.GetId(),
            organizerCreateDto.Name,
            organizerCreateDto.Description
        );

        await _organizerRepository.InsertAsync(organizer);

        return ObjectMapper.Map<Organizer, OrganizerDto>(organizer);
    }

    public async Task<PagedResultDto<OrganizerListElementDto>> GetListAsync(OrganizerFilterDto organizerFilterDto)
    {
        var (totalCount, organizers) = await _organizerRepository.GetPagedListAsync(
            organizerFilterDto.OwnerId,
            organizerFilterDto.Name,
            organizerFilterDto.SkipCount,
            organizerFilterDto.MaxResultCount
        );

        return new PagedResultDto<OrganizerListElementDto>(
            totalCount,
            ObjectMapper.Map<List<Organizer>, List<OrganizerListElementDto>>(organizers)
        );
    }

    public async Task<OrganizerProfileDto> GetProfileDtoAsync(string name)
    {
        var organizer = await _organizerRepository.GetAsync(x => x.Name == name);
        var organizerProfileDto = ObjectMapper.Map<Organizer, OrganizerProfileDto>(organizer);

        var owner = await _userRepository.GetAsync(organizer.UserId);
        organizerProfileDto.OwnerUserName = owner.UserName;
        organizerProfileDto.OwnerEmail = owner.Email;

        return organizerProfileDto;
    }

    public async Task<ListResultDto<OrganizerListElementDto>> GetOrganizersByUserIdAsync(Guid userId)
    {
        var organizers = await _organizerRepository.GetOwnedOrganizersAsync(userId);
        var organizerList = ObjectMapper.Map<List<Organizer>, List<OrganizerListElementDto>>(organizers);
        return new ListResultDto<OrganizerListElementDto>(organizerList);
    }

    [Authorize]
    public async Task<bool> IsOrganizerOwnerAsync(Guid organizerId)
    {
        return await _organizerRepository
            .AnyAsync(x => 
                x.Id == organizerId && x.UserId == CurrentUser.GetId());
    }

    [Authorize]
    public async Task UpdateAsync(Guid id, OrganizerUpdateDto organizerUpdateDto)
    {
        var organizer = await _organizerRepository.GetAsync(id);

        if (organizer.UserId != CurrentUser.GetId())
        {
            throw new AbpAuthorizationException(EventsDomainErrorCodes.NotAllowToUpdateOrganizer);
        }
        
        organizer.SetName(organizerUpdateDto.Name);
        organizer.SetDescription(organizerUpdateDto.Description);

        await _organizerRepository.UpdateAsync(organizer);
    }
}