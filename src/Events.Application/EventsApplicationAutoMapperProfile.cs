﻿using AutoMapper;
using Events.Categories;
using Events.Organizers;
using Events.Organizers.Employers;
using Volo.Abp.Identity;

namespace Events;

public class EventsApplicationAutoMapperProfile : Profile
{
    public EventsApplicationAutoMapperProfile()
    {
        CreateMap<Category, CategoryDto>();

        CreateMap<Organizer, OrganizerListElementDto>();
        CreateMap<Organizer, OrganizerProfileDto>();
        CreateMap<Organizer, OrganizerDto>();
        CreateMap<IdentityUser, OrganizerEmployeeDto>();
    }
}
