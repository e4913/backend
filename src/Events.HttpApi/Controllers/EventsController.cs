﻿using Events.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Events.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class EventsController : AbpControllerBase
{
    protected EventsController()
    {
        LocalizationResource = typeof(EventsResource);
    }
}
