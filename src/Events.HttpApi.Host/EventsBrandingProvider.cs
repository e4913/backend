﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Events;

[Dependency(ReplaceServices = true)]
public class EventsBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "Events";
}
