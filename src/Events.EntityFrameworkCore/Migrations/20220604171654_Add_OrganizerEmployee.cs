﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Events.Migrations
{
    public partial class Add_OrganizerEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "E_OrganizerEmployees",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    OrganizerId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_E_OrganizerEmployees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_E_OrganizerEmployees_AbpUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_E_OrganizerEmployees_E_Ogranizers_OrganizerId",
                        column: x => x.OrganizerId,
                        principalTable: "E_Ogranizers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_E_OrganizerEmployees_OrganizerId",
                table: "E_OrganizerEmployees",
                column: "OrganizerId");

            migrationBuilder.CreateIndex(
                name: "IX_E_OrganizerEmployees_UserId",
                table: "E_OrganizerEmployees",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "E_OrganizerEmployees");
        }
    }
}
