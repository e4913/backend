﻿using Events.Categories;
using Events.Events;
using Events.Organizers;
using Events.Organizers.Employers;
using Events.Venues;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Identity;

namespace Events.EntityFrameworkCore;

public static class EventsDbContextModelBuilderExtensions
{
    public static void ConfigureEvents(this ModelBuilder builder)
    {
        //todo add indexes
        builder.Entity<Category>(x =>
        {
            x.ToTable(InterpolateTableName("Categories"));
            x.ConfigureByConvention();

            x.Property(y => y.Name)
                .HasMaxLength(CategoryConstants.MaxNameLength)
                .IsRequired();
        });

        builder.Entity<Venue>(x =>
        {
            x.ToTable(InterpolateTableName("Venues"));
            x.ConfigureByConvention();

            x.Property(y => y.Name)
                .HasMaxLength(VenueConstants.MaxNameLength)
                .IsRequired();
        });

        builder.Entity<Organizer>(x =>
        {
            x.ToTable(InterpolateTableName("Ogranizers"));
            x.ConfigureByConvention();

            x.Property(y => y.Name)
                .HasMaxLength(OrganizerConstants.MaxNameLength)
                .IsRequired();

            x.Property(y => y.Description)
                .HasMaxLength(OrganizerConstants.MaxDescriptionLength)
                .IsRequired();

            x.HasOne<IdentityUser>()
                .WithMany()
                .HasForeignKey(y => y.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        });

        builder.Entity<OrganizerEmployee>(x =>
        {
            x.ToTable(InterpolateTableName("OrganizerEmployees"));
            x.ConfigureByConvention();

            x.HasOne<Organizer>()
                .WithMany()
                .HasForeignKey(y => y.OrganizerId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            x.HasOne<IdentityUser>()
                .WithMany()
                .HasForeignKey(y => y.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        });

        builder.Entity<Event>(x =>
        {
            x.ToTable(InterpolateTableName("Events"));
            x.ConfigureByConvention();

            x.Property(y => y.Title)
                .HasMaxLength(EventConstants.MaxTitleLength)
                .IsRequired();

            x.Property(y => y.Description)
                .HasMaxLength(EventConstants.MaxDescriptionLength)
                .IsRequired();

            x.HasOne<Organizer>()
                .WithMany()
                .HasForeignKey(y => y.OrganizerId)
                .IsRequired();

            x.HasOne<Venue>()
                .WithMany()
                .HasForeignKey(y => y.VenueId)
                .IsRequired();

            x.HasMany(y => y.Categories)
                .WithOne()
                .HasForeignKey(y => y.EventId)
                .IsRequired();
        });

        builder.Entity<EventCategory>(x =>
        {
            x.ToTable(InterpolateTableName("EventCategories"));
            x.ConfigureByConvention();

            x.HasKey(y => new { y.EventId, y.CategoryId });

            x.HasOne<Event>()
                .WithMany(y => y.Categories)
                .HasForeignKey(y => y.EventId)
                .IsRequired();

            x.HasOne<Category>()
                .WithMany()
                .HasForeignKey(y => y.CategoryId)
                .IsRequired();
        });
    }

    private static string InterpolateTableName(string name) =>
        $"{EventsConstants.DbTablePrefix}{name}{EventsConstants.DbSchema}";
}