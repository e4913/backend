using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Events.Events;
using Events.Organizers;
using Events.Venues;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Events.EntityFrameworkCore.Events;

public class EventRepository : EfCoreRepository<EventsDbContext, Event, Guid>, IEventRepository
{
    public EventRepository(IDbContextProvider<EventsDbContext> dbContextProvider) : base(dbContextProvider)
    {
    }

    public async Task<int> GetCountAsync(
        string? title = null,
        string? organizerName = null,
        string? venueName = null,
        CancellationToken cancellationToken = default)
    {
        var dbContext = await GetDbContextAsync();

        var eventQueryable = await GetQueryableAsync();
        var organizationQueryable = dbContext.Set<Organizer>().AsQueryable();
        var venueQueryable = dbContext.Set<Venue>().AsQueryable();

        var query = (from @event in eventQueryable
                join organizer in organizationQueryable on @event.OrganizerId equals organizer.Id
                join venue in venueQueryable on @event.VenueId equals venue.Id
                select new
                {
                    Event = @event,
                    Organizer = organizer,
                    Venue = venue
                })
            .WhereIf(!string.IsNullOrWhiteSpace(title),
                x => Contains(x.Event.Title, title!))
            .WhereIf(!string.IsNullOrWhiteSpace(organizerName),
                x => Contains(x.Organizer.Name, organizerName!))
            .WhereIf(!string.IsNullOrWhiteSpace(venueName),
                x => Contains(x.Venue.Name, venueName!));

        return await query.CountAsync(GetCancellationToken(cancellationToken));
    }

    public async Task<(int, List<EventWithDetails>)> GetPagedListAsync(
        string? sorting = null,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        string? title = null,
        string? organizerName = null,
        string? venueName = null,
        CancellationToken cancellationToken = default)
    {
        var dbContext = await GetDbContextAsync();

        var eventQueryable = await GetQueryableAsync();
        var organizerQueryable = dbContext.Set<Organizer>().AsQueryable();
        var venueQueryable = dbContext.Set<Venue>().AsQueryable();

        var query = (from @event in eventQueryable
            join organizer in organizerQueryable on @event.OrganizerId equals organizer.Id
            join venue in venueQueryable on @event.VenueId equals venue.Id
            select new EventWithDetails
            {
                Id = @event.Id,
                Title = @event.Title,
                Description = @event.Description,
                OrganizerName = organizer.Name,
                VenueName = venue.Name
            })
            .WhereIf(!string.IsNullOrWhiteSpace(title), 
                x => Contains(x.Title, title!))
            .WhereIf(!string.IsNullOrWhiteSpace(organizerName),
                x => Contains(x.OrganizerName, organizerName!))
            .WhereIf(!string.IsNullOrWhiteSpace(venueName),
                x => Contains(x.VenueName, venueName!))
            .OrderBy(string.IsNullOrWhiteSpace(sorting) ? EventConstants.DefaultSorting : sorting);

        var totalCountTask = query.CountAsync(GetCancellationToken(cancellationToken));
        var eventsTask = query
            .PageBy(skipCount, maxResultCount)
            .ToListAsync(GetCancellationToken(cancellationToken));

        await Task.WhenAll(totalCountTask, eventsTask);

        var totalCount = await totalCountTask;
        var events = await eventsTask;
        
        return (totalCount, events);
    }

    private static bool Contains(string right, string left)
    {
        return right.ToLower().Contains(left.ToLower());
    }
}