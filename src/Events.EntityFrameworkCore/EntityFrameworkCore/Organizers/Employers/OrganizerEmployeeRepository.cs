﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Events.Organizers;
using Events.Organizers.Employers;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Identity;

namespace Events.EntityFrameworkCore.Organizers.Employers;

public class OrganizerEmployeeRepository : EfCoreRepository<EventsDbContext, OrganizerEmployee, Guid>,
    IOrganizerEmployeeRepository
{
    public OrganizerEmployeeRepository(IDbContextProvider<EventsDbContext> dbContextProvider) : base(dbContextProvider)
    {
    }

    public async Task<int> GetCountAsync(
        Guid? organizerId,
        Guid? userId,
        CancellationToken cancellationToken = default)
    {
        var organizerEmployeeQueryable = await GetQueryableAsync();

        var query = organizerEmployeeQueryable
            .WhereIf(organizerId.HasValue,
                x => x.OrganizerId == organizerId)
            .WhereIf(userId.HasValue,
                x => x.UserId == userId);
        return await query.CountAsync(GetCancellationToken(cancellationToken));
    }

    public async Task<List<OrganizerEmployeeWithDetails>> GetListAsync(
        Guid? organizerId,
        Guid? userId,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default)
    {
        var dbContext = await GetDbContextAsync();

        var organizerEmployeeDbSet = dbContext.Set<OrganizerEmployee>();
        var organizerDbSet = dbContext.Set<Organizer>();
        var userDbSet = dbContext.Set<IdentityUser>();

        var query = organizerEmployeeDbSet
            .Join(organizerDbSet,
                x => x.OrganizerId,
                x => x.Id,
                (x, y) =>
                    new { OrganizerEmployee = x, Organizer = y })
            .Join(userDbSet,
                x => x.OrganizerEmployee.UserId,
                user => user.Id,
                (organizerEmployee, user) =>
                    new { organizerEmployee, user })
            .WhereIf(userId.HasValue,
                x => x.user.Id == userId)
            .WhereIf(organizerId.HasValue,
                x => x.organizerEmployee.Organizer.Id == organizerId)
            .OrderByDescending(x =>
                x.organizerEmployee.OrganizerEmployee.CreationTime)
            .Select(x => new OrganizerEmployeeWithDetails
            {
                OrganizerName = x.organizerEmployee.Organizer.Name,
                EmployeeUserName = x.user.UserName,
                Id = x.organizerEmployee.OrganizerEmployee.Id
            });

        return await query
            .PageBy(skipCount, maxResultCount)
            .ToListAsync(GetCancellationToken(cancellationToken));
    }

    public async Task<(int, List<IdentityUser>)> GetEmployersAsync(Guid organizerId,
        int skipCount = 0,
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default)
    {
        var dbContext = await GetDbContextAsync();

        var organizerEmployeeQueryable = await GetQueryableAsync();
        var userQueryable = dbContext.Set<IdentityUser>().AsQueryable();

        var query = from organizerEmployee in organizerEmployeeQueryable
            join user in userQueryable on organizerEmployee.UserId equals user.Id
            where organizerEmployee.OrganizerId == organizerId
            orderby organizerEmployee.CreationTime descending
            select user;

        var totalCountTask = query.CountAsync(GetCancellationToken(cancellationToken));
        var usersTask = query.PageBy(skipCount, maxResultCount).ToListAsync(GetCancellationToken(cancellationToken));
        await Task.WhenAll(totalCountTask, usersTask);

        var totalCount = await totalCountTask;
        var users = await usersTask;
        
        return (totalCount, users);
    }
}