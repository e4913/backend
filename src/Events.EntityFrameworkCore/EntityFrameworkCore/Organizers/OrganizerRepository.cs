﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Events.Organizers;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Events.EntityFrameworkCore.Organizers;

public class OrganizerRepository : EfCoreRepository<EventsDbContext, Organizer, Guid>, IOrganizerRepository
{
    public OrganizerRepository(IDbContextProvider<EventsDbContext> dbContextProvider) : base(dbContextProvider)
    {
    }

    public async Task<(int, List<Organizer>)> GetPagedListAsync(
        Guid? ownerId, 
        string? name, 
        int skipCount = 0, 
        int maxResultCount = int.MaxValue,
        CancellationToken cancellationToken = default)
    {
        var organizerQueryable = await GetQueryableAsync();

        var query = organizerQueryable
            .WhereIf(ownerId.HasValue,
                x => x.UserId == ownerId)
            .WhereIf(!string.IsNullOrWhiteSpace(name), 
                x => x.Name.ToLower().Contains(name.ToLower()));
        
        var totalCountTask = query.CountAsync(GetCancellationToken(cancellationToken));
        var organizersTask = query.PageBy(skipCount, maxResultCount).ToListAsync(GetCancellationToken(cancellationToken));

        await Task.WhenAll(totalCountTask, organizersTask);

        var totalCount = await totalCountTask;
        var organizers = await organizersTask;

        return (totalCount, organizers);
    }

    public async Task<List<Organizer>> GetOwnedOrganizersAsync(Guid ownerId, CancellationToken cancellationToken = default)
    {
        var organizerQueryable = await GetQueryableAsync();
        var organizers = await organizerQueryable
            .Where(x => x.UserId == ownerId)
            .ToListAsync(GetCancellationToken(cancellationToken));
        return organizers;
    }
}