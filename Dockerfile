FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /

COPY ["common.props", "/"]
COPY ["src/Events.Application/Events.Application.csproj", "src/Events.Application/"]
COPY ["src/Events.DbMigrator/Events.DbMigrator.csproj", "src/Events.DbMigrator/"]
COPY ["src/Events.Domain/Events.Domain.csproj", "src/Events.Domain/"]
COPY ["src/Events.EntityFrameworkCore/Events.EntityFrameworkCore.csproj", "src/Events.EntityFrameworkCore/"]
COPY ["src/Events.HttpApi/Events.HttpApi.csproj", "src/Events.HttpApi/"]
COPY ["src/Events.Application.Contracts/Events.Application.Contracts.csproj", "src/Events.Application.Contracts/"]
COPY ["src/Events.Domain.Shared/Events.Domain.Shared.csproj", "src/Events.Domain.Shared/"]
COPY ["src/Events.HttpApi.Client/Events.HttpApi.Client.csproj", "src/Events.HttpApi.Client/"]
COPY ["src/Events.HttpApi.Host/Events.HttpApi.Host.csproj", "src/Events.HttpApi.Host/"]

RUN dotnet restore "src/Events.HttpApi.Host/Events.HttpApi.Host.csproj"
RUN dotnet restore "src/Events.DbMigrator/Events.DbMigrator.csproj"

COPY . .

WORKDIR "/src/Events.HttpApi.Host"
RUN dotnet publish "Events.HttpApi.Host.csproj" -c Release -o /backend/publish

WORKDIR "/src/Events.DbMigrator"
RUN dotnet publish "Events.DbMigrator.csproj" -c Release -o /migrator/publish

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS migrator
WORKDIR /src/Events.DbMigrator

COPY --from=build /migrator/publish /src/Events.DbMigrator
COPY --from=build /src/Events.EntityFrameworkCore/Migrations /src/Events.EntityFrameworkCore/Migrations
COPY --from=build /Events.sln /Events.sln

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /src/Events.DbMigrator
USER appuser

ENTRYPOINT ["dotnet", "Events.DbMigrator.dll"]

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS backend
WORKDIR /backend

COPY --from=build /backend/publish .

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /backend
USER appuser

ENV ASPNETCORE_URLS=http://+:8000
EXPOSE 8000


ENTRYPOINT ["dotnet", "Events.HttpApi.Host.dll"]
