﻿using Volo.Abp.Modularity;

namespace Events;

[DependsOn(
    typeof(EventsApplicationModule),
    typeof(EventsDomainTestModule)
    )]
public class EventsApplicationTestModule : AbpModule
{

}
