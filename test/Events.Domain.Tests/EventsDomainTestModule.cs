﻿using Events.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Events;

[DependsOn(
    typeof(EventsEntityFrameworkCoreTestModule)
    )]
public class EventsDomainTestModule : AbpModule
{

}
